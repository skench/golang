package main

import (
	"fmt"
)

func main() {
	var Name string
	var age int
	fmt.Println("Укажите ваше Имя")
	fmt.Scan(&Name) // Считываем значение указанное пользователем в переменную Name
	fmt.Println("Укажите ваш Возраст")
	fmt.Scan(&age) // Считываем значение указанное пользователем в переменную age
	fmt.Println("I'm", Name)
	fmt.Println("I'm", age, "years old")
	fmt.Println("Looking forward to the next lesson!")
}
