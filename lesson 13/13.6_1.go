/*
Напишите функцию, которая принимает в качестве аргументов два числа типа int, вычисляет сумму чётных чисел заданного диапазона и выводит результат в консоль.
Рекомендация

Если первое число, переданное в качестве аргумента, будет больше второго, просто поменяйте их местами.
*/
package main

import "fmt"

func main() {
	sumTwoNumbers(5, 3)
}

func sumTwoNumbers(a, b int64) {
	if a < 0 || b < 0 {
		fmt.Println("Введеные числа не корректны")
		fmt.Println("Введеные числа меньше нуля.")
	} else if a > b {
		a, b = b, a
		result := a * b
		fmt.Println(result)
	} else {
		result := a * b
		fmt.Println(result)
	}
}
