/*
Задание 1. Определение количества слов, начинающихся с большой буквы
Что нужно сделать

Напишите программу, которая выведет количество слов, начинающихся с большой буквы в строке: Go is an Open source programming Language that makes it Easy to build simple, reliable, and efficient Software.
*/

package main

import (
	"fmt"
	"unicode"
)

func main() {
	var capitalLettersCount = 0
	arr := make([]string, 0)
	fmt.Println("Введите ваше предложение, если предложение законцено, ведите 0, чтение данных будет завершено")

	for true {
		var x string
		fmt.Scan(&x)
		if x == "exit" {
			break
		}
		arr = append(arr, x)
	}
	for i := 0; i < len(arr); i++ {
		var word = arr[i]
		if unicode.IsUpper([]rune(word)[0]) {
			capitalLettersCount++
		}
	}
	fmt.Printf("В тексте %v слов с заглавной буквы", capitalLettersCount)
}
