package main

import (
	"fmt"
	"strings"
	"unicode"
)

func main() {

	// f is a function which returns true if the
	// c is number and false otherwise
	f := func(c rune) bool {
		return unicode.Is(c)
	}

	// FieldsFunc() function splits the string passed
	// on the return values of the function f
	// String will therefore be split when a number
	// is encontered and returns all non-numbers
	fmt.Printf("Fields are: %d\n",
		strings.FieldsFunc("ABC123PQR456XYZ789", f))
}
