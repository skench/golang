package main

import (
	"fmt"
)

func main() {
	message := sayHello("Максим", 21)
	fmt.Println(message)

	fmt.Println(increment())
	fmt.Println(increment())
	fmt.Println(increment())
	fmt.Println(increment())

	fmt.Println(increment2())
	fmt.Println(increment2())
	fmt.Println(increment2())
	fmt.Println(increment2())
}

func sayHello(name string, age int) string {
	return fmt.Sprintf("Привет, %s! Тебе %d", name, age)
}

func increment() func() int {
	count := 0
	return func() int {
		count++
		return count
	}
}

func increment2() int {
	count := 0
	count++
	return count
}
