/*
Одним из видов компьютерного искусства является псевдографика, когда из символов создаются картины. Попробуйте вывести два изображения. Запросите у пользователя размер шахматной доски в клетках и выведите шахматную доску на экран. Белые поля выведите звёздочкой, а чёрные — пробелом.
*/

package main

import (
	"fmt"
)

func main() {
	var row, col string = "|_|", "|‾|"
	var height int = 7
	var width int = 7

	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			for j < width {
				fmt.Printf("%s", col)
				j++
				if j == width {
					break
				}
				fmt.Printf("%s", row)
				j++
				if j == width {
					break
				}
				if j == width {
					fmt.Println()
				}
			}
			if i == height {
				break
			}
			fmt.Println()
		}
	}
}
