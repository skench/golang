package main

import (
	"fmt"
	"strconv"
)

func main() {

	type myBox struct {
		min int
		max int
	}

	info := myBox{
		min: 100000,
		max: 999999,
	}
	var sumer int = 0
	for i := info.min; i <= info.max; i++ {
		str := strconv.Itoa(i)

		middlePoint := len(str) / 2
		remainder := len(str) % middlePoint

		if remainder != 0 {
			continue
		}

		firstPart := make([]rune, 0)
		lastPart := make([]rune, 0)

		for idx, r := range str {
			if idx < middlePoint {
				firstPart = append(firstPart, r)
			} else {
				lastPart = append(lastPart, r)
			}
		}

		for i, j := 0, len(lastPart)-1; i < j; i, j = i+1, j-1 {
			lastPart[i], lastPart[j] = lastPart[j], lastPart[i]
		}

		if string(firstPart) == string(lastPart) {

			sumer = sumer + 1
			//fmt.Println(i, "is mirrored")
			fmt.Println("Кол-во зеркальных билетов", sumer)
		}

	}

}
