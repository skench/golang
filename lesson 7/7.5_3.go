package main

import (
	"fmt"
	"strings"
)

func main() {
	lenTree := 5

	for i := 1; i <= lenTree; i++ {
		fmt.Println(strings.Repeat(" ", lenTree-i) + strings.Repeat("*", i) + strings.Repeat("*", i-1))
	}
}
