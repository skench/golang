/*
Задание 4. Область видимости переменных
Что нужно сделать

Напишите программу, в которой будет три функции, попарно использующие разные глобальные переменные. Функции должны прибавлять к поданному на вход числу глобальную переменную и возвращать результат. Затем вызовите по очереди три функции, передавая результат из одной в другую.
*/

package main

import "fmt"

const (
	a = 100
	b = 200
	c = 300
)

func f1(integer int) int {
	return integer + a
}

func f2(integer int) int {
	return integer + b
}

func f3(integer int) int {
	return integer + c
}

func main() {
	var integer int

	fmt.Print("Пожалуйста, введите число: ")
	fmt.Scan(&integer)

	fmt.Printf("Результат первой функции: %v\n", f1(integer))
	fmt.Printf("Результат второй функции: %v\n", f2(integer))
	fmt.Printf("Результат третьей функции: %v\n", f3(integer))

	fmt.Printf("Результат последовательного сложения всех цифр: %v\n", f3(f2(f1(integer))))
}
