/*
Задание 3. Именованные возвращаемые значения
Что нужно сделать

Напишите программу, которая на вход получает число, затем с помощью двух функций преобразует его. Первая умножает, а вторая прибавляет число, используя именованные возвращаемые значения.
*/

package main

import "fmt"

func main() {
	f1(3, 3)
	f2(4, 6)

}

func f1(a, b int) (f1Number int) {

	f1Number = a * b
	fmt.Println(f1Number)
	return

}

func f2(a, b int) (f2Number int) {
	f2Number = a + b
	fmt.Println(f2Number)
	return
}
