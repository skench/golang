/*
Напишите программу, которая в цикле складывает 4 произвольных числа. Программа должна, в теле цикла, спрашивать у пользователя новое число, прибавлять его к уже полученной сумме и после 4-х итераций — выводить сумму на экран.
*/

package main

import "fmt"

func main() {

	var firstNumber, secondNumber, thirdNumber, fourdNumber, fivedNumber int = 0, 0, 0, 0, 0

	//sum := firstNumber + secondNumber + thirdNumber + fourdNumber
	//fmt.Println(sum)

	var i int = 0
	for i < 4 {
		fmt.Println("Введите значение первой переменной")
		fmt.Scanln(&firstNumber)
		i++
		fmt.Println("Введите значение второй переменной")
		fmt.Scanln(&secondNumber)
		i++
		fmt.Println("Введите значение третьей переменной")
		fmt.Scanln(&thirdNumber)
		i++
		fmt.Println("Введите значение четвертой переменной")
		fmt.Scanln(&fourdNumber)
		i++
	}
	sum := firstNumber + secondNumber + thirdNumber + fourdNumber
	fmt.Println("Сумма введенных вами чисел равна =", sum)

	fmt.Println("Введите значение пятой переменной")
	fmt.Scanln(&fivedNumber)
	for i := 0; i < 4; i++ {
		sum = sum + fivedNumber
	}
	fmt.Println("Сумма чисел после 4х итераций с новой переменной", sum)
}
