package main

import (
	"fmt"
)

func main() {
	var number, degree int
	var i int = 0
	fmt.Println("Программа  возводит введеное вами число в степень, степень так же указывается пользователем")
	fmt.Println("Введите число которое хотите возвести в степень")
	fmt.Scanln(&number)
	fmt.Println("Введите в какую степень необходимо возвести ранее указанного числа")
	fmt.Scanln(&degree)
	result := number
	degree = degree - 1
	for i < degree {
		result *= number
		fmt.Println(result)
		i = i + 1
	}

}
