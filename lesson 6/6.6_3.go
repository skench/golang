/*
Задание 3. Расчёт суммы скидки
Напишите программу, которая принимает на вход цену товара и скидку. Посчитайте и верните на экран сумму скидки. Скидка должна быть не больше 30% от цены товара и не больше 2000 рублей.
*/

package main

import "fmt"

func main() {

	type MyBox struct {
		Price    int
		Discount int
	}

	var array [3]MyBox
	for index := range array {
		info := MyBox{}
		fmt.Println("Введите стоимость товара")
		fmt.Scanln(&info.Price)
		fmt.Println("Введите размер скидки")
		fmt.Scanln(&info.Discount)

		array[index] = info
	}

	for _, info := range array {
		fmt.Println(info)
	}

	var sumDiscount int = 0
	for _, info := range array {
		sumDiscount += info.Discount
	}
	fmt.Println(sumDiscount)

	var sumPrice int = 0
	for _, info := range array {
		sumPrice += info.Price
	}
	fmt.Println(sumPrice)

	limit := sumPrice * sumDiscount / 100
	fmt.Println(limit)

	if limit > 2000 {
		fmt.Println("Ваша скидка достигла", limit, ",но в магазине установлена максимальная сумма скидки в 2000 рублей")
	} else if sumDiscount > 30 {
		fmt.Println("Ваша скидка достигла", sumDiscount, ",но в магазине установлена максимальная сумма скидки в 30%")
	} else {
		fmt.Println("Типа все ок, мы прошли проверку")
	}

}
