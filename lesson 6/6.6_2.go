/*
Задание 2. Сумма двух чисел по единице
Напишите программу, которая запрашивает у пользователя два числа и складывает их в цикле следующим образом: берёт первое число и прибавляет к нему по единице, пока не достигнет суммы двух чисел.
*/

package main

import (
	"fmt"
	"os"
)

func main() {
	var firstNumber, secondNumber int
	fmt.Println("Введите первое целочисленное число")
	fmt.Fscan(os.Stdin, &firstNumber)
	fmt.Println("Введите второе целочисленное число")
	fmt.Fscan(os.Stdin, &secondNumber)
	sum := firstNumber + secondNumber
	for firstNumber < sum {
		firstNumber = firstNumber + 1
	}
	fmt.Println(firstNumber)
}
