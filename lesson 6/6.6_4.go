/*
Задание 4. Предыдущее занятие на if
Что нужно сделать

Есть три переменные со значениями 0. Первую нужно наполнить до 10, вторую — до 100, третью — до 1000. Напишите цикл, в котором эти переменные будут увеличиваться на один.
Рекомендация

Используйте условия для пропуска переменных, которые уже достигли своих лимитов.
*/

package main

import "fmt"

func NewSlice(start, end, step int) []int {
	if step <= 0 || end < start {
		return []int{}
	}
	s := make([]int, 0, 1+(end-start)/step)
	for start <= end {
		s = append(s, start)
		start += step
	}
	return s
}

func main() {

	fmt.Println(NewSlice(0, 10, 1))
	fmt.Println(NewSlice(0, 100, 1))
	fmt.Println(NewSlice(0, 1000, 1))
}
