package main

import (
	"fmt"
	"time"
)

func main() {
	var guestCount int = 0
	var summa int = 0
	weekday := time.Now().Weekday()
	fmt.Println(weekday) // "Tuesday"
	fmt.Println("Укажите число гостей")
	fmt.Scan(&guestCount)
	fmt.Println("Укажите сумму чека")
	fmt.Scan(&summa)
	if weekday == 1 {
		fmt.Println("Понедельник день тяжелый, ваша скидка 10%")
		summa = (summa * 10) / 100
		fmt.Println(summa)
	} else if weekday == 5 && summa > 10000 {
		fmt.Println("Ваша скидка 5%")
		summa = (summa * 5) / 100
		fmt.Println(summa)
	} else if guestCount > 5 {
		f := float64(summa)
		c := 1.1
		fmt.Println("Включена надбавка за обслуживание 10%")
		f = f * c
		fmt.Println(f)
	} else {
		fmt.Println("Сумма чека", summa)
	}
}
