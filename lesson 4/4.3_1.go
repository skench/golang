/*Напишите программу, которая ищет минимум из двух чисел.
Подсказка: учтите, что числа могут быть равны!
*/
package main

import "fmt"

func main () {
	var firstNumber, secNumber int
	fmt.Println("Введите 1е число:")
	fmt.Scanf("%d\n", &firstNumber)
	fmt.Println("Введите 2е число:")
	fmt.Scanf("%d\n", &secNumber)
	fmt.Println(firstNumber, secNumber)
	if firstNumber < secNumber {
		fmt.Println("2е число больше 1го")
	} else if firstNumber == secNumber {
		fmt.Println("Числа равны")
	} else {
		fmt.Println("1е число больше 2го")
	}
}
