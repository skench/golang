//Задача 1. Калькулятор скидки
//Вы покупаете 3 товара в магазине. Если сумма вашего чека превышает 10 000 руб., вам будет сделана скидка 10%.
//Напишите программу, которая запрашивает 3 стоимости товара и вычисляет сумму чека.
package main

import (
	"fmt"
)

func main() {
	var vase, box, lamp int
	discount := 10
	fmt.Println("Введите стоимость вазы:")
	fmt.Scanf("%d\n", &vase)
	//var box int
	fmt.Println("Введите стоимость шкатулки:")
	fmt.Scanf("%d\n", &box)
	//var lamp int
	fmt.Println("Введите стоимость лампы:")
	fmt.Scanf("%d\n", &lamp)

	cost := vase + box + lamp

	if cost >= 10000 {
		x := cost / 100 * discount
		total := cost - x
		fmt.Println("Ваша цена с учетом скидки 10%:", total)
	} else if cost <= 9999 {
		fmt.Println("Ваша цена:", cost)
	}

}
