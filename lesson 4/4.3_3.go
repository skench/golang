package main

import "fmt"

func main() {
    fmt.Printf("Введите число: ")
    var i int32
    _, err := fmt.Scanf("%d", &i)
    if err != nil {
        fmt.Printf("%v\n", err)
        // maybe a good time to exit
    }
    if i%2 == 0 {
        fmt.Print("Введеное вами число четное")
    } else {
        fmt.Println("Введеное вами число нечетное")
    }
}