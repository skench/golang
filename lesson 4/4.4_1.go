/*
Для пилотируемой миссии на Марс отобрали троих космонавтов. 
Беда в том, что они оказались настолько похожи, что выбрать командира экипажа оказалось затруднительно. 
Тогда решили проверить их IQ. Напишите программу, которая запрашивает IQ трёх космонавтов и назначает командира по наивысшему IQ.
*/

package main

import (
	"fmt"
	"os"
)

func main() {
	var ivanovAA, sidorovAA, petrovAA int

	fmt.Printf("Введите IQ Иванов А.А: ")
	fmt.Fscan(os.Stdin, &ivanovAA)
	fmt.Printf("Введите IQ Сидоров А.А: ")
	fmt.Fscan(os.Stdin, &sidorovAA)
	fmt.Printf("Введите IQ Петров А.А: ")
	fmt.Fscan(os.Stdin, &petrovAA)
	values := []int{ivanovAA, sidorovAA, petrovAA}

	max := values[0]
	for _, v := range values {
		if v > max {
			max = v
		}
	}
	fmt.Println("Командиром будет косманавт с IQ: ", max)
}