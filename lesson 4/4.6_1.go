package main

import (
	"fmt"
	"os"
)

func main() {
	passingPoints := 275
	var algebra, russianLanguage, physics int = 0, 0, 0
	fmt.Fscan(os.Stdin, &algebra)
	fmt.Fscan(os.Stdin, &russianLanguage)
	fmt.Fscan(os.Stdin, &physics)
	result := (algebra + russianLanguage + physics)
	if result < passingPoints {
		fmt.Println("Вы не набрали необходимого кол-ва балов")

	} else {
		fmt.Println("Поздравляю, вы зачислены в Слизерин")
	}
}
