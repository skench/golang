//Задача 3. Модуль числа
//Напишите программу, которая вычисляет модуль числа.
//Подсказка: чтобы обратить знак числа в переменной ‘x’ надо писать вот так:
//x = -x
package main

import (
	"fmt"
	"math"
	"os"
)

func main() {
	var number float64
	fmt.Print("Введите отрицательное число, чтобы вывести его модуль: ")
	fmt.Fscan(os.Stdin, &number)
	result := math.Abs(number)
	fmt.Println(result)
}
