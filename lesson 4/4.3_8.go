/*
В отделе маркетинга работают 3 сотрудника.
У каждого — разные зарплаты. Напишите программу, которая вычисляет разницу между самой высокой
и низкой зарплатой сотрудника, а также среднюю зарплату отдела.
*/

package main

import (
	"fmt"
	"os"
)

func main() {
	var ivanovAA, sidorovAA, petrovAA int

	fmt.Printf("Введите какую ЗП получает Иванов А.А: ")
	fmt.Fscan(os.Stdin, &ivanovAA)
	fmt.Printf("Введите какую ЗП получает Сидоров А.А: ")
	fmt.Fscan(os.Stdin, &sidorovAA)
	fmt.Printf("Введите какую ЗП получает Петров А.А: ")
	fmt.Fscan(os.Stdin, &petrovAA)
	values := []int{ivanovAA, sidorovAA, petrovAA}

	min := values[0]
	for _, v := range values {
		if v < min {
			min = v
		}
	}

	max := values[0]
	for _, g := range values {
		if g > max {
			max = g
		}
	}

	average := (ivanovAA + petrovAA + sidorovAA) / 3  //Вычисляем среднее значение
	difference := max - min // Вычисляем разницу между максимальной и минимальной ЗП
	fmt.Println(difference)
	fmt.Println(average)
}
