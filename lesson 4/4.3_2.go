/*Напишите программу, которая проверяет то, как вы умеете складывать два числа в уме.
Программа должна выводить два разных сообщения на верный и неверный ответ пользователя.
В последнем случае, надо показывать правильный результат.
Пользователь должен ввести с клавиатуры два целых числа, а потом результат их сложения.
*/
package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main() {
	var a int
	rand.Seed(time.Now().UTC().UnixNano())
	var firstRandom, secondRandom int
	firstRandom = rand.Intn(100)
	secondRandom = rand.Intn(100)
	fmt.Println(firstRandom)
	fmt.Println(secondRandom)
	sum := firstRandom + secondRandom
	fmt.Printf("Введите сколько будет сложив %v со вторым числом %v\n", firstRandom, secondRandom)
	fmt.Fscan(os.Stdin, &a)
	if sum == a {
		fmt.Println("ваш ответ верный")
	} else if sum != a {
		fmt.Println("Ваш ответ неверный")
		fmt.Println("Правильный ответ ", sum)
	}
}
