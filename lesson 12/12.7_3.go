/*
Что нужно сделать

Напишите программу, создающую текстовый файл только для чтения, и проверьте, что в него нельзя записать данные.
Рекомендация

Для проверки создайте файл, установите режим только для чтения, закройте его, а затем, открыв, попытайтесь прочесть из него данные.
*/
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

func main() {

	f, err := os.Create("12.7_3.txt")
	t := time.Now()
	if err != nil {
		fmt.Println(err)
		return
	}
	if err := os.Chmod("12.7_3.txt", 0444); err != nil {
		fmt.Println("Ошибка определения уровня доступа: ", err)
		return
	}
	defer f.Close()
	arr := make([]string, 0)
	fmt.Println("Введите ваше предложение, если предложение законцено, ведите exit, чтение данных будет завершено")
	for {
		var x string
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		x = scanner.Text()
		if x == "exit" {
			break
		}

		arr = append(arr, x)
		if err != nil {
			fmt.Println("Ошибка записи", err)
		}
	}

	_, err = f.WriteString(t.Format("2006.01.02 15:04:05") + " " + strings.Join(arr, "\n"))
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	f.Close()
	fmt.Println("Opening a file ")
	f, err = os.Open("12.7_3.txt")
	if err != nil {
		fmt.Println("Ошибка открытия файла", err)
		return
	}
	buf := make([]byte, 1256)
	if _, err := io.ReadFull(f, buf); err != nil {
		fmt.Println("Не смогли прочитать последовательность", err)
		return
	}
	fmt.Printf("%s\n", buf)
}
