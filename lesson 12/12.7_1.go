/*
Напишите программу, которая на вход получала бы строку, введённую пользователем, а в файл писала № строки, дату и сообщение в формате:

2020-02-10 15:00:00 продам гараж.

При вводе слова exit программа завершает работу.
*/
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	f, err := os.Create("12.7_1.txt")
	t := time.Now()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	arr := make([]string, 0)
	fmt.Println("Введите ваше предложение, если предложение законцено, ведите 0, чтение данных будет завершено")
	for {
		var x string
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		x = scanner.Text()
		if x == "exit" {
			break
		}
		arr = append(arr, x)
	}
	_, err = f.WriteString(t.Format("2006.01.02 15:04:05") + " " + strings.Join(arr, "\n"))
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
}
