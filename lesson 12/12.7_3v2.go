package main

import (
	"bufio"
	"fmt"

	//"io"
	"os"
	"time"
)

func main() {

	f, err := os.Create("12.7_3.txt")

	if err != nil {
		fmt.Println(err)
		return
	}
	if err := os.Chmod("12.7_3.txt", 0444); err != nil {
		fmt.Println("Ошибка определения уровня доступа: ", err)
		return
	}
	defer f.Close()
	arr := make([]string, 0)
	fmt.Println("Введите ваше предложение, если предложение законцено, ведите exit, чтение данных будет завершено")
	for {
		var x string
		t := time.Now()
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		x = scanner.Text()
		if x == "exit" {
			break
		}
		tf := t.Format("2006.01.02 15:04:05")
		endText := tf + " " + x + "\n"
		arr = append(arr, endText)
		fmt.Println(arr)
	}
}
