package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	var fileName = "file.txt"

	f, err := os.Open(fileName)
	if err != nil {
		fmt.Println("Ошибка открытия файла: ", err)
		return
	}
	defer f.Close()

	resultBytes, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Println("Ошибка чтения файла: ", err)
		return
	}
	fmt.Println(string(resultBytes))
}
