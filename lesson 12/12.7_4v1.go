package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	i := 1
	var b bytes.Buffer
	var fileName = "file.txt"

	file, err := os.Create(fileName)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	for {
		var text string

		fmt.Print("Введите, пожалуйста, текст: ")
		fmt.Scan(&text)

		if strings.ToLower(text) == "exit" {
			fmt.Print("Выход из программы...\n")
			break
		} else {
			currentTime := time.Now().Format("2006-01-02 15:04:05")
			b.WriteString(strconv.Itoa(i) + ". " + currentTime + " " + text + "\n")
		}
		i++
	}

	if err := ioutil.WriteFile(fileName, b.Bytes(), 0666); err != nil {
		fmt.Println("Ошиибка записи в файл: ", err)
	}
}
