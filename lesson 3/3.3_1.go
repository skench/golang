package main

import (
	"fmt"
)

func main() {
	var name string
	var password string
	var age int

	fmt.Println("Введите ваше имя")
	fmt.Scan(&name)
	fmt.Println("Введите ваш пароль")
	fmt.Scan(&password)
	fmt.Println("Введите ваш возраст")
	fmt.Scan(&age)
	fmt.Println("Поздравляю,", name, ", теперь вы зарегистрированы!")
	fmt.Println("Ваш пароль: ", password)
	fmt.Println("Ваш возраст: ", age)
}
