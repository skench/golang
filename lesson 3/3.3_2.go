package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	var name string
	var password string
	var age int

	fmt.Println("Введите ваше имя")
	fmt.Scan(&name)
	fmt.Println("Введите ваш пароль")
	fmt.Scan(&password)
	fmt.Println("Введите ваш возраст")
	fmt.Scan(&age)
	s := fmt.Sprintf("введенный логин %s введенный пароль %s введенный возраст %d вы успешно зашли\n", name, password, age)
	io.WriteString(os.Stdout, s)
}
