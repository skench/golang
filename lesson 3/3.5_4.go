package main

import "fmt"

func main() {
	startHeight := 100
	growth := 50
	losses := 20
    finalHeight := startHeight + ( growth - losses ) * 2 + growth / 2
	fmt.Println("Высота бамбука к середине третьего дня", finalHeight)
}
