package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	var a int
	fmt.Println("Введите число, квадрат которого необходимо вычислить")
	fmt.Scan(&a)
	s := fmt.Sprintf("Вы ввели %d Квадрат введеного вами числа %d", a, a*a)
	io.WriteString(os.Stdout, s)
}
