package main

import (
	"fmt"
)

func main() {
	var a int
	fmt.Println("Введите число, квадрат которого необходимо вычислить")
	fmt.Scan(&a)
	result := a * a
	fmt.Println("Вы ввели", a, "Квадрат введеного вами числа", result)
}
