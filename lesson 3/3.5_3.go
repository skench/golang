package main

import (
	"fmt"
)

func main() {

	a := 42
	b := 153

	c := a
	a = b
	b = c
	fmt.Println("a:", a)
	fmt.Println("b:", b)

}
