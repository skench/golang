package main

import "fmt"

func main() {
	startHeight := 100 //при старте высота саженца
	growth := 50 // выростает за день
	losses := 20 // съедают гусеницы за ночь
	maxHeight := 300 //максимальная высота
    y := 1 //счетчик дней
    day := 30 //результат вычитания growth - losses
    firstDay := startHeight + (growth - losses) //результат 1 дня
    for firstDay < maxHeight{
    	y++
    	firstDay += day
    	fmt.Println("Потребуется дней, чтобы саженец вырос до максимальной высоты",y)
    }
}