/*
Пользователь вводит будний день недели в сокращённой форме (пн, вт, ср, чт, пт) и получает развёрнутый список всех последующих рабочих дней, включая пятницу.
*/

package main

import (
	"fmt"
)

func main() {

	for {
		var daysWeek string
		fmt.Print("Дни недели.\n")
		fmt.Println("Введите будний день недели: пн, вт, ср, чт, пт: ")
		_, err := fmt.Scanf("%s", &daysWeek)
		if err != nil {
			fmt.Println("err:", err)
			continue
		}
		switch daysWeek {
		case "пн":
			fmt.Print("Понедельник\n")
			fallthrough
		case "вт":
			fmt.Println("Вторник\n")
			fallthrough
		case "ср":
			fmt.Println("Среда\n")
			fallthrough
		case "чт":
			fmt.Println("Четверг\n")
			fallthrough
		case "пт":
			fmt.Println("Пятница\n")
			fallthrough
		default:
			fmt.Println("Вы указали некорректный день недели", daysWeek, "Повторите")
			return
		}

	}
}
