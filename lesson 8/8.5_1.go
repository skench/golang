/*
Пользователь вводит месяц, программа должна вывести, на какое время года (зиму, весну, лето, осень) этот месяц выпадает.

Как группировать:

    декабрь, январь, февраль — зима;
    март, апрель, май — весна;
    июнь, июль, август — лето;
    сентябрь, октябрь, ноябрь — осень.
*/

package main

import (
	"fmt"
)

func main() {

	for {
		var season string
		fmt.Print("Времена года.\nВведите месяц: ")
		_, err := fmt.Scanf("%s", &season)
		if err != nil {
			fmt.Println("Invalid you: err:", err)
			continue
		}
		switch season {
		case "Декабрь", "Январь", "Февраль":
			fmt.Println("Время года —", "Зима")
			return
		case "Март", "Апрель", "Май":
			fmt.Println("Время года —", "Весна")
			return
		case "Июнь", "Июль", "Август":
			fmt.Println("Время года —", "Лето")
			return
		case "Сентябрь", "Октябрь", "Ноябрь":
			fmt.Println("Время года —", "Осень")
			return
		default:
			fmt.Println("Вы указали некорректное время года", season, "Повторите")
			return
		}

	}
}
