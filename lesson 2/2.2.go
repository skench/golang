package main

import (
	"fmt"
)

//Сайт по продаже недвижимости. Переменная содержит стоимость квартиры.
var priceApartment int = 2666777

//Программа для учета товара в обувном магазине. Переменная содержит размер пары обуви.
var shoeSize float32 = 36.5

//Информационная система для посетителей зоопарка. Переменная содержит количество животных, которые находятся в зоопарке в данный момент.
var countAnimalsZoo int = 120

//Интернет магазин электроники. Переменная для цены нового товара.
var newProductPrice float64 = 1200.64

func main() {
	fmt.Println(priceApartment)
	fmt.Println(shoeSize)
	fmt.Println(countAnimalsZoo)
	fmt.Println(newProductPrice)
}

// Short variable declarations can only be used inside functions. You have to write
