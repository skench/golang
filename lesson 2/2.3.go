package main

import (
	"fmt"
)

var lapRace int = 4
var speedRacer int = 358
var engine int = 254
var wheels int = 93
var steeringWheel int = 49
var wind int = 21
var rain int = 17

func main() {
	fmt.Println("===================")
	fmt.Println("Супер гонки. Круг", lapRace)
	fmt.Println("===================")
	fmt.Print("Шумахер ", (speedRacer), "\n")
	fmt.Println("===================")
	fmt.Println("Водитель: Шумахер")
	fmt.Print("Скорость: ", speedRacer, "\n")
	fmt.Println("-------------------")
	fmt.Println("Оснащение")
	fmt.Print("Двигатель: +", engine, "\n")
	fmt.Print("Колеса: +", wheels, "\n")
	fmt.Print("Руль: +", steeringWheel, "\n")
	fmt.Println("-------------------")
	fmt.Println("Действия плохой погоды")
	fmt.Print("Ветер: -", wind, "\n")
	fmt.Print("Дождь: -", rain, "\n")
	
}
