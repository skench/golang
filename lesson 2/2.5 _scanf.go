package main

import (
	"fmt"
)

func main() {
	var engine int
    var wind int
	var rain int
	fmt.Println("===================")
	fmt.Println("Введите мощность двигателя")
	fmt.Scan(&engine)
	fmt.Println("===================")
	fmt.Println("Укажите значения губительно влияющие на скорость машины")
	fmt.Println("Ветер")
	fmt.Scan(&wind)
	fmt.Println("Дождь")
	fmt.Scan(&rain)
	lap := 4
	wheels := 93
	steeringWheel := 49
	var speed int = ( engine + wheels + steeringWheel) - ( wind + rain )

	fmt.Println("===================")
	fmt.Print("Супер гонки. Круг ", lap, "\n")
	fmt.Println("===================")
	fmt.Print("Шумахер (", speed, ")\n")
	fmt.Println("===================")
	fmt.Println("Водитель: Шумахер")
	fmt.Print("Скорость: ", speed, "\n")
	fmt.Println("-------------------")
	fmt.Println("Оснащение")
	fmt.Print("Двигатель: +", engine, "\n")
	fmt.Print("Колеса: +", wheels, "\n")
	fmt.Print("Руль: +", steeringWheel, "\n")
	fmt.Println("-------------------")
	fmt.Println("Действия плохой погоды")
	fmt.Print("Ветер: -", wind, "\n")
	fmt.Print("Дождь: -", rain, "\n")
}
