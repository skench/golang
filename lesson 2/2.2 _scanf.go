package main

import (
	"fmt"
)

func main() {
	//Сайт по продаже недвижимости. Переменная содержит стоимость квартиры.
var priceApartment int64

//Программа для учета товара в обувном магазине. Переменная содержит размер пары обуви.
var shoeSize float32

//Информационная система для посетителей зоопарка. Переменная содержит количество животных, которые находятся в зоопарке в данный момент.
var countAnimalsZoo int

//Интернет магазин электроники. Переменная для цены нового товара.
var newProductPrice float32
	fmt.Println("Укажите стоимость квартиры")
	fmt.Scan(&priceApartment)
	fmt.Println("Укажите размер обуви")
	fmt.Scan(&shoeSize)
	fmt.Println("Укажите кол-во животных в Зоопарке")
	fmt.Scan(&countAnimalsZoo)
	fmt.Println("Укажите цену нового товара")
	fmt.Scan(&newProductPrice)
	fmt.Println("Стоимость квартиры")
	fmt.Println(priceApartment)
	fmt.Println("Размер обуви")
	fmt.Println(shoeSize)
	fmt.Println("Кол-во животных в Зоопарке")
	fmt.Println(countAnimalsZoo)
	fmt.Println("Цена нового товара")
	fmt.Println(newProductPrice)
}

// Short variable declarations can only be used inside functions. You have to write
