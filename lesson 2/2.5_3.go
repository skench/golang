package main

import (
	"fmt"
)

func main() {
	var workShiftDuration int = 480
	var timeOrder int = 2
	var collectionOrder int = 4
	var result = workShiftDuration / ( timeOrder + collectionOrder)
	
	
	fmt.Println("===================")
	fmt.Print("Эта программа рассчитает, сколько клиентов успеет обслужить кассир за смену.\n")
	fmt.Println("===================")
	fmt.Print("Введите длительность смены в минутах: ", workShiftDuration, "\n")
	fmt.Println("===================")
	fmt.Print("Сколько минут клиент делает заказ? ", timeOrder, "\n")
	fmt.Println("===================")
	fmt.Print("Сколько минут кассир собирает заказ? ", collectionOrder, "\n")
	fmt.Print("За смену длиной ", workShiftDuration, " минут кассир успеет обслужить ", result, "\n")
}
