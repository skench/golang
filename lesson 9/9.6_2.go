package main

import (
	"fmt"
	"math"
)

func main() {
	numberOne := 0
	numberTwo := 0
	var result int64

	for {
		fmt.Print("Пожалуйста, введите первое число: ")
		fmt.Scan(&numberOne)
		if numberOne > math.MaxInt16 {
			fmt.Print("Число должно умещаться в тип int16!\n")
		} else {
			break
		}
	}

	for {
		fmt.Print("Пожалуйста, введите второе число: ")
		fmt.Scan(&numberTwo)
		if numberTwo > math.MaxInt16 {
			fmt.Print("Число должно умещаться в тип int16!\n")
		} else {
			break
		}
	}

	result = int64(numberTwo * numberOne)

	switch result {
	case result <= math.MaxUint8:
		fmt.Print("Минимальный тип - uint8")
	case result <= math.MaxUint16:
		fmt.Print("Минимальный тип - uint16")
	case result == math.MaxUint32:
		fmt.Print("Минимальный тип - uint32")
	case result >= math.MinInt8:
		fmt.Print("Минимальный тип - int8")
	case result >= math.MinInt16:
		fmt.Print("Минимальный тип - int16")
	case result == math.MinInt32:
		fmt.Print("Минимальный тип - int32")
	}

	if result >= 0 {
		if result <= math.MaxUint8 {
			fmt.Print("Минимальный тип - uint8")
		} else if result <= math.MaxUint16 {
			fmt.Print("Минимальный тип - uint16")
		} else {
			fmt.Print("Минимальный тип - uint32")
		}
	} else {
		if result >= math.MinInt8 {
			fmt.Print("Минимальный тип - int8")
		} else if result >= math.MinInt16 {
			fmt.Print("Минимальный тип - int16")
		} else {
			fmt.Print("Минимальный тип - int32")
		}
	}
}
