package main

import (
	"fmt"
	"unsafe"
)

var (
	ToBe      bool   = false
	maxInt    int    = 1<<63 - 1
	minInt    int    = -1<<63 - 0
	maxByte   byte   = 1<<8 - 1
	maxRune   rune   = 1<<31 - 1
	maxInt8   int8   = 1<<7 - 1
	maxUInt8  uint8  = 1<<8 - 1
	maxInt16  int16  = 1<<15 - 1
	maxUInt16 uint16 = 1<<16 - 1
	maxInt32  int32  = 1<<31 - 1
	maxUInt32 uint32 = 1<<32 - 1
	maxInt64  int64  = 1<<63 - 1
	maxUInt64 uint64 = 1<<64 - 1
)

func main() {
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", ToBe, unsafe.Sizeof(ToBe), ToBe)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxInt, unsafe.Sizeof(maxInt), maxInt)
	fmt.Printf("Тип : -%T Размер: %d в байтах. Значение %v\n", minInt, unsafe.Sizeof(minInt), minInt)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxByte, unsafe.Sizeof(maxByte), maxByte)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxRune, unsafe.Sizeof(maxRune), maxRune)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxInt8, unsafe.Sizeof(maxInt8), maxInt8)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxUInt8, unsafe.Sizeof(maxUInt8), maxUInt8)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxInt16, unsafe.Sizeof(maxInt16), maxInt16)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxUInt16, unsafe.Sizeof(maxUInt16), maxUInt16)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxInt32, unsafe.Sizeof(maxInt32), maxInt32)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxUInt32, unsafe.Sizeof(maxUInt32), maxUInt32)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxInt64, unsafe.Sizeof(maxInt64), maxInt64)
	fmt.Printf("Тип : %T Размер: %d в байтах. Значение %v\n", maxUInt64, unsafe.Sizeof(maxUInt64), maxUInt64)
}
