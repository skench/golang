package main

import "fmt"

func main() {
	fmt.Println(twoSum([6]int{7, 2, 11, 15}, 9))
}

func twoSum(nums [6]int, target int) []int {
	//result := make([]int, 0, 2)
	result := []int{}

	for start := 0; start < len(nums); start++ {
		for end := start + 1; end < len(nums); end++ {
			if (nums[start] + nums[end]) == target {
				return []int{start, end}
			}
		}
	}

	return result
}
