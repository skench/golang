package main

import "fmt"

func main() {
	primes := [6]int{11, 7, 2, 15}
	var target int = 9
	for i := 0; i < len(primes); i++ {
		for j := i + 1; j < len(primes); j++ {
			if primes[i]+primes[j] != target {
				continue
			} else if primes[i]+primes[j] == target {
				fmt.Println(primes[i], primes[j])
			} else {
				fmt.Println("Искомая комбинация не найдена")
			}
		}
	}
}
