package main

import "fmt"

func singleNonDuplicate(nums []int) int {
	i := 0
	for i < len(nums) {
		if nums[i] == nums[i+1] {
			i = i + 2
		} else {
			return nums[i]
		}
	}
	return nums[i]
}

func main() {
	fmt.Println(singleNonDuplicate([]int{1, 1, 2, 3, 3, 4, 4, 8, 8}))
}