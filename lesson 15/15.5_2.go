/*
Задание 2. Функция, реверсирующая массив
Что нужно сделать

    Напишите функцию, принимающую на вход массив и возвращающую массив, в котором элементы идут в обратном порядке по сравнению с исходным.
    Напишите программу, демонстрирующую работу этого метода.

Что оценивается

При вводе 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 программа должна выводить при помощи дополнительной функции, реверсировав массив: 10, 9, 8, 7, 6, 5, 4, 3, 2, 1.
*/

package main

import (
	"fmt"
)

func reverseArray(inArray []int) []int {
	inArrayLength := len(inArray)

	for i := 0; i < len(inArray)/2; i++ {
		inArray[i], inArray[inArrayLength-1-i] = inArray[inArrayLength-1-i], inArray[i] //меняем местами элементы в слайсе, первый элемент становится последним, второй элемент, предпоследним
	}
	return inArray
}

func main() {
	var length int
	fmt.Print("Пожалуйста, введите длину массива: ")
	fmt.Scan(&length)

	myslice := make([]int, length) //используем slice.
	for i := 0; i < length; i++ {
		fmt.Printf("Введите элемент [%v] = ", i+1)
		fmt.Scan(&myslice[i])
	}
	fmt.Print(reverseArray(myslice))
	//	fmt.Println(reflect.ValueOf(myslice)) //показать как на самом деле в слайсе лежат введенные данные (эксперимент)
}
