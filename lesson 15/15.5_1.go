/*
Задание 1. Подсчёт чётных и нечётных чисел в массиве
Что нужно сделать

Разработайте программу, позволяющую ввести 10 целых чисел, а затем вывести из них количество чётных и нечётных чисел. Для ввода и подсчёта используйте разные циклы.
Что оценивается

Для введённых чисел 1, 1, 1, 2, 2, 2, 3, 3, 3, 4 программа должна вывести: чётных — 4, нечётных — 6.
*/

package main

import (
	"fmt"
)

func chetnechet(a [10]int) (chet, nechet int) {
	var lenght = len(a)
	for i := 0; i < lenght; i++ {
		if a[i]%2 == 0 {
			chet++
		} else {
			nechet++
		}
	}
	return
}

func main() {
	fmt.Println("Введите 10 чисел\nВам необходимо ввести натуральное число и подтвердить ввод числа клавишей 'Enter'\nЧисла с плавающей точкой вводить нельзя")
	var array [10]int
	for i := 0; i < 10; i++ {
		var tempTrashForArray int
		fmt.Scan(&tempTrashForArray)
		array[i] = tempTrashForArray
	}
	chet, nechet := chetnechet(array)
	fmt.Printf("Вы ввели\nЧетных %d чисел и %d нечетных\n чисел", chet, nechet)
}
