/*
Для пилотируемой миссии на Марс отобрали троих космонавтов. 
Беда в том, что они оказались настолько похожи, что выбрать командира экипажа оказалось затруднительно. 
Тогда решили проверить их IQ. Напишите программу, которая запрашивает IQ трёх космонавтов и назначает командира по наивысшему IQ.


Первая задача решена практически верно. Сделайте проверку на отрицательные значения и на лимит в 100 баллов. Жду эту правку:)
*/



package main

import (
        "fmt"
        "os"
)

type Employee struct {
        firstName  string
        secondName string
        iq         uint8
}

func main() {
        var maxIQ uint8 = 100

        e1 := Employee{}

        fmt.Printf("Введите Имя первого космонавта ")
        fmt.Fscan(os.Stdin, &e1.firstName)
        fmt.Printf("Введите Фамилию первого космонавта ")
        fmt.Fscan(os.Stdin, &e1.secondName)
        fmt.Printf("Введите IQ первого космонавта ")
        fmt.Fscan(os.Stdin, &e1.iq)


        if e1.iq > maxIQ {
            fmt.Println("Введено IQ больше максимального")
            return
        }

        e2 := Employee{}

        fmt.Printf("Введите Имя второго космонавта ")
        fmt.Fscan(os.Stdin, &e2.firstName)
        fmt.Printf("Введите Фамилию второго космонавта ")
        fmt.Fscan(os.Stdin, &e2.secondName)
        fmt.Printf("Введите IQ второго космонавта ")
        fmt.Fscan(os.Stdin, &e2.iq)
        if e2.iq > maxIQ {
            fmt.Println("Введено IQ больше максимального")
            return
        }

        e3 := Employee{}

        fmt.Printf("Введите Имя третьего космонавта ")
        fmt.Fscan(os.Stdin, &e3.firstName)
        fmt.Printf("Введите Фамилию третьего космонавта ")
        fmt.Fscan(os.Stdin, &e3.secondName)
        fmt.Printf("Введите IQ третьего космонавта ")
        fmt.Fscan(os.Stdin, &e3.iq)
        if e3.iq > maxIQ {
            fmt.Println("Введено IQ больше максимального")
            return
        }

        if e1.iq == e2.iq {
                fmt.Println("У космонавта", e1.secondName, e1.firstName, " и ", e2.secondName, e2.firstName, " набрано одинаковое кол-во IQ")
        } else if e2.iq == e3.iq {
                fmt.Println("У космонавта", e2.secondName, e2.firstName, " и ", e3.secondName, e3.firstName, " набрано одинаковое кол-во IQ")
        } else if e1.iq == e3.iq {
                fmt.Println("У космонавта", e1.secondName, e1.firstName, " и ", e3.secondName, e3.firstName, " набрано одинаковое кол-во IQ")
        }

        fmt.Println(e1)
        fmt.Println(e2)
        fmt.Println(e3)
}
