/*
Доп. задание:

    Напишите программку, которой вы подаете на вход 2 строки. Программа проверяет, содержится ли вторая строка в первой или нет. Для этого вам поможет одна из функций в пакете strings.
*/

package main

import (
	"fmt"
	"strings"
)

func main() {
	var firstString, secondString string
	fmt.Println("Введите первое слово/фразу слитно, без пробелов")
	fmt.Scan(&firstString)
	fmt.Println("Введите второе слово/фразу слитно, без пробелов")
	fmt.Scan(&secondString)
	itog := (strings.Contains(firstString, secondString))
	if itog == true {
		fmt.Println("Второе слово/фраза есть в первом слове/фразе")
	} else {
		fmt.Println("Второе слово/фраза нет в первом слове/фразе")
	}
}
