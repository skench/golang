/*
Проверка пользовательского ввода на различные ограничения является частой задачей. Попросите пользователя ввести три числа и проверьте, что хотя бы одно является положительным. Результат проверки необходимо сообщить пользователю.
*/

package main

import (
	"fmt"
	"os"
)

func main() {
	var x, y, z int
	negativNumber := -1
	fmt.Printf("Введите значение переменно X ")
	fmt.Fscan(os.Stdin, &x)
	fmt.Printf("Введите значение переменно Y ")
	fmt.Fscan(os.Stdin, &y)
	fmt.Printf("Введите значение переменной Z ")
	fmt.Fscan(os.Stdin, &z)

	if negativNumber > x || x == 0 {
		fmt.Println("Введено отрицательное значение", x)
	} else {
		fmt.Println("Введено положительное значение", x)
	}

	if negativNumber > y || y == 0 {
		fmt.Println("Введено отрицательное значение", y)
	} else {
		fmt.Println("Введено положительное значение", y)
	}

	if negativNumber > z || z == 0 {
		fmt.Println("Введено отрицательное значение", z)
	} else {
		fmt.Println("Введено положительное значение", z)
	}
}