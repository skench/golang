/*
1) Реализуйте программку, в которой вы вводите 3 строки и программка печатает их в алфавитном порядке на экране.
*/

package main

import (
      "fmt"
      "sort"
	  "os"
)

func main() {
	  var e1Word, e2Word, e3Word string

	  fmt.Printf("Введите первое слово ")
	  fmt.Fscan(os.Stdin, &e1Word)
	  fmt.Printf("Введите первое слово ")
	  fmt.Fscan(os.Stdin, &e2Word)
	  fmt.Printf("Введите первое слово ")
	  fmt.Fscan(os.Stdin, &e3Word)

      s := []string{e1Word, e2Word, e3Word}
      sort.Strings(s)
      fmt.Println(s)
}