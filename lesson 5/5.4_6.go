/*
Задание 6. Счастливый билет
Что нужно сделать

Напишите программу, в которую пользователь будет вводить четырёхзначный номер билета, а программа будет выводить, является ли он зеркальным, счастливым или обычным билетом.
Советы и рекомендации

При решении задачи необходимо применить целочисленное деление и остаток от деления. Примеры вывода программы:

1234 -> обычный билет 

3425 -> счастливый билет 

1221 -> зеркальный билет
*/

package main

import (
    "fmt"
	"os"
)

type Employee struct {
	ticket  uint
}

func main() {
	t1 := Employee{}
	fmt.Printf("Введите первую цифру билета ")
	fmt.Fscan(os.Stdin, &t1.ticket)
	t2 := Employee{}
	fmt.Printf("Введите вторую цифру билета ")
	fmt.Fscan(os.Stdin, &t2.ticket)
	t3 := Employee{}
	fmt.Printf("Введите третью цифру билета ")
	fmt.Fscan(os.Stdin, &t3.ticket)
	t4 := Employee{}
	fmt.Printf("Введите четвертую цифру билета ")
	fmt.Fscan(os.Stdin, &t4.ticket)

	sumT1_T2 := t1.ticket + t2.ticket
	sumT3_T4 := t3.ticket + t4.ticket



	if t1.ticket == t4.ticket || t2.ticket == t3.ticket {
		fmt.Println ("Поздравляю! У Вас зеркальный билет")
	} else if sumT1_T2 == sumT3_T4 {
		fmt.Println("Поздравляю! У Вас счастливый билет")
	} else {
		fmt.Println("У Вас обычный билет")
	}
}