/*
Задание 7 (по желанию). Игра «Угадай число»
Что нужно сделать

Ну и какой же компьютер без игр? Давайте научим его играть в «Угадай число». Пользователь загадывает число от 1 до 10 (включительно). Программа пытается это число угадать, для этого она выводит число, а пользователь должен ответить: угадала программа, больше загаданное число или меньше.
Рекомендация

Программа не должна делать больше четырёх попыток в процессе угадывания.
*/

package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func main() {
	fmt.Printf("Добро пожаловать в игру 'Угадай число'\nВы загадываете число от 1 до 10, а я его постараюсь угадать.\n")
	rand.Seed(time.Now().UTC().UnixNano())
	var answer string
	yes := "yes"
	no := "no"

	fmt.Println("Это число больше 5?")
	fmt.Fscan(os.Stdin, &answer)
	if answer == yes {
		for i := 0; i < 5; i++ {
			fmt.Println(randInt(5, 11))
		}
	} else if answer == no {
		fmt.Println(randInt(1, 6))
	} else {
		fmt.Println("Вы ввели не корректный ответ на вопрос")
	}
}
